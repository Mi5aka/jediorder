from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^jediorder/', include('jediorder.urls', namespace='jediorder')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
