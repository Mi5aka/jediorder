from django.contrib import admin

from .models.questions import Question, Answer, Quiz
from .models.candidate import Candidate
from .models.planet import Planet
from .models.jedi import Jedi


class PlanetAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name', 'id']


class JediAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'planet'
        )
    list_filter = ['planet']
    search_fields = ['name', 'id']


class CandidateAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'age',
        'planet', 'email', 'padawan'
        )
    list_filter = ['planet', 'padawan', 'age']
    search_fields = ['name', 'planet', 'email']


class QuizAdmin(admin.ModelAdmin):
    list_display = ['candidate_id']


class QuestionAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'question'
        )
    search_fields = ['question']


class AnswerAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display = (
        'id', 'question', 'quiz', 'answer'
    )


admin.site.register(Jedi, JediAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(Planet, PlanetAdmin)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
