import os


# Email settings
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'smtp.yandex.com')
EMAIL_PORT = int(os.environ.get('EMAIL_PORT', '465'))
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')
EMAIL_USE_SSL = bool(os.environ.get('EMAIL_USE_SSL', 'True'))

SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

DB_MAILER_ALLOWED_MODELS_ON_ADMIN = [
    'MailFromEmailCredential',
    'MailFromEmail',
    'MailTemplate',
    'MailLog',
]
