from django.conf.urls import url
from jediorder.views import JediListView, CandidateCreateView

from . import views


urlpatterns = [
    url(r'^jedi/$', JediListView.as_view(), name='jedi'),
    url(r'^candidate/$', CandidateCreateView.as_view(), name='candidate'),
    url(r'^candidate/new/$', views.new_candidate_view, name='create-candidate'),
    url(r'^quiz/(?P<id>[0-9]+)/$', views.quiz_view, name='padawan-quiz'),
    url(r'^quiz/new/$', views.create_result_quiz_view, name='create-quiz'),
    url(r'^jedi/(?P<id>[0-9]+)/$', views.candidates_in_padawan_view, name='candidates-in-padawan'),
    url(r'^jedi/padawan/$', views.choice_padawan_view, name='choice-padawan'),
]
