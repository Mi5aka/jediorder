from django.db import models
from django.utils.translation import ugettext_lazy as _


class Planet(models.Model):
    name = models.CharField(max_length=25, verbose_name=_("Наименование"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Планета")
        verbose_name_plural = _("Планеты")
