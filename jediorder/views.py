from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView
from django.views.decorators.csrf import csrf_exempt

from dbmail import send_db_mail
from .models.questions import Answer, Question, Quiz
from .models.candidate import Candidate
from .models.planet import Planet
from .models.jedi import Jedi


class JediListView(ListView):

    context_object_name = 'jedi_list'
    queryset = Jedi.objects.all().select_related('planet').order_by('planet')
    template_name = 'jediorder/jedi_list.html'


def candidates_in_padawan_view(request, id):
    quiz_list = Quiz.objects.all()
    questions = Question.objects.filter(id__in=quiz_list)
    candidates = Candidate.objects.filter(
        planet__id=id, padawan=False,
        id__in=quiz_list.values('candidate_id')
    )
    results = Answer.objects.select_related('quiz', 'question').filter(
        quiz__in=quiz_list
    )
    return render(request, 'jediorder/candidates_in_padawans.html', {
        'candidates': candidates,
        'results': results,
        'questions': questions

    })


@csrf_exempt
def choice_padawan_view(request):
    candidate_id = request.POST.get('candidate_id')
    candidate = Candidate.objects.get(id=candidate_id)
    candidate.padawan = True
    candidate.save()

    context = {'username': candidate.name}

    send_db_mail(
        'template-for-padawans', candidate.email,
        context, use_celery=True
    )

    return render(request, 'jediorder/candidates_in_padawans.html')


class CandidateCreateView(CreateView):
    model = Candidate
    fields = ['name', 'age', 'planet', 'email']


@csrf_exempt
def new_candidate_view(request):
    id = []
    if request.method == 'POST':
        name = request.POST.get('name')
        age = request.POST.get('age')
        planet = request.POST.get('planet')
        email = request.POST.get('email')
        new_candidate = Candidate.objects.create(
            name=name, age=age, email=email,
            planet=Planet.objects.get(id=planet)
        )
        id.append(new_candidate.id)
    candidate = Candidate.objects.get(id=id[0])
    return redirect('jediorder:padawan-quiz', id=candidate.id)


@csrf_exempt
def quiz_view(request, id):
    questions = Question.objects.all()
    candidate = Candidate.objects.get(id=id)
    return render(request, 'jediorder/quiz.html', {
        'questions': questions, 'candidate': candidate
    })


@csrf_exempt
def create_result_quiz_view(request):
    data = {}
    for key, value in request.POST.items():
        data[key] = value
    candidate = Candidate.objects.get(id=data['candidate_id'])
    questions = Question.objects.all()

    if Quiz.objects.filter(candidate_id=candidate.id).exists() is True:
        for question in questions:
            Answer.objects.create(
                question=question, answer=data[str(question.id)],
                quiz=Quiz.objects.get(candidate_id=candidate.id)
            )
    else:
        quiz = Quiz(candidate_id=candidate)
        quiz.save()
        for question in questions:
            quiz.questions.add(question.id)
            quiz.save()
        for question in questions:
            Answer.objects.create(
                question=question, answer=data[str(question.id)],
                quiz=quiz
            )

    return render(request, 'jediorder/end_test.html')
