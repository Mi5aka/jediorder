from django.db import models
from django.utils.translation import ugettext_lazy as _


class Jedi(models.Model):
    name = models.CharField(
        max_length=25, verbose_name=_("Имя")
    )
    planet = models.ForeignKey(
        'jediorder.Planet', verbose_name=_("Планета обучения")
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Джедай")
        verbose_name_plural = _("Джедаи")
