# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'bootstrap3',
    'dbmail',
    'djcelery',
    'django_redis',
    'redis',
]

LOCAL_APPS = [
    'config',
    'jediorder',
]

INSTALLED_APPS.extend(LOCAL_APPS)
