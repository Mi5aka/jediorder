// функция передает параметр при зачислении кандидата в падаваны

$('#button_padawan').click(function() {
    var qs = document.querySelector('#data'),
        attribute = qs.dataset;
    var candidate_id = attribute.id;
    var data = {candidate_id: candidate_id};
    $.ajax({
        url: attribute.url,
        method: 'POST',
        data: data,
        complete: function(){
            setTimeout(function() {window.location.reload();}, 1000);
        }
    });
});

// функция для отображения результатов испытияния кандидатов
$('[id^=b]').click(function() {
    var result = 'r' + this.id;
    $(this).hide("fast");
    $('#' + result).show('fast');
});
