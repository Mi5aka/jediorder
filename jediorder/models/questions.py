from django.db import models
from django.utils.translation import ugettext_lazy as _


class Question(models.Model):
    question = models.TextField(verbose_name=_("Вопрос"))

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = _("Вопрос")
        verbose_name_plural = _("Вопросы")


class Answer(models.Model):
    answer = models.NullBooleanField(verbose_name=_("Ответ"))
    question = models.ForeignKey(
        'jediorder.Question', verbose_name=_("Вопрос")
    )
    quiz = models.ForeignKey(
        'jediorder.Quiz', verbose_name=_("Тестовое испытание")
    )

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Ответ")
        verbose_name_plural = _("Ответы")


class Quiz(models.Model):
    candidate_id = models.ForeignKey(
        'jediorder.Candidate', verbose_name=_("Код ордена")
    )
    questions = models.ManyToManyField(
        'jediorder.Question', verbose_name=_("Вопросы")
    )

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Тестовое испытание")
        verbose_name_plural = _("Тестовые испытания")
