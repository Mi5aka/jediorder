from django.db import models
from django.utils.translation import ugettext_lazy as _


class Candidate(models.Model):
    name = models.CharField(
        max_length=25, verbose_name=_("Имя"),
    )
    planet = models.ForeignKey(
        'jediorder.Planet', verbose_name=_("Планета обитания"),
    )
    age = models.IntegerField(
        verbose_name=_("Возраст")
    )
    email = models.EmailField(
        verbose_name=_("Email")
    )
    padawan = models.BooleanField(default=False, verbose_name=_("Падаван?"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Кандидат")
        verbose_name_plural = _("Кандидаты")
