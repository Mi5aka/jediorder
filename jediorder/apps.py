from django.apps import AppConfig


class JediorderConfig(AppConfig):
    name = 'jediorder'
